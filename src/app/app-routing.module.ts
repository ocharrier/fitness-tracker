import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome/welcome.component';
import {AuthGuard} from './auth/auth.guard';

const routes: Routes = [
    {path: '', component: WelcomeComponent},
    // Lazy Loading
    {path: 'training', loadChildren: './training/training.module#TrainingModule', canLoad: [AuthGuard]}

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    // Service available to whole application but only used in routing module
    providers: [AuthGuard]
})
export class AppRoutingModule {

}
