import {AuthData} from './auth-data.model';

import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {TrainingService} from '../training/training.service';
import {UiService} from '../shared/ui.service';
import {Store} from '@ngrx/store';
// Convention for naming reducer
import * as fromRoot from '../app.reducer';
import * as UI from '../shared/ui.actions';
import {SetAuthenticated, SetUnauthenticated} from './auth.actions';

@Injectable()
export class AuthService {


    constructor(private router: Router,
                private afAuth: AngularFireAuth,
                private trainingService: TrainingService,
                private uiService: UiService,
                private store: Store<fromRoot.State>) {

    }

    initAuthListener() {
        console.log(this.router.url);
        this.afAuth.authState.subscribe(user => {
            if (user) {
                this.store.dispatch(new SetAuthenticated());

                this.router.navigate(['/training']);
            } else {
                this.store.dispatch(new SetUnauthenticated());
                this.trainingService.cancelSubscriptions();
                // Don't redirect if homepage
                if (this.router.url !== '' && this.router.url !== '/') {

                    this.router.navigate(['/login']);
                }
            }
        });
    }

    registerUser(authData: AuthData) {
        // this.uiService.loadingStateChanged.next(true);
        this.store.dispatch(new UI.StartLoading());
        this.afAuth.auth.createUserWithEmailAndPassword(authData.email, authData.password).then(result => {
            console.log(result);
            // this.uiService.loadingStateChanged.next(false);
            this.store.dispatch(new UI.StopLoading());
        }).catch(error => {
            console.log(error);
            // this.uiService.loadingStateChanged.next(false);
            this.store.dispatch(new UI.StopLoading());
            this.uiService.showSnackBar(error.message, null, {
                duration: 3000
            });
        });


    }

    login(authData: AuthData) {
        this.store.dispatch(new UI.StartLoading());
        this.afAuth.auth.signInWithEmailAndPassword(authData.email, authData.password).then(result => {
            console.log(result);

            this.store.dispatch(new UI.StopLoading());
        }).catch(error => {
            console.log(error);

            this.store.dispatch(new UI.StopLoading());
            this.uiService.showSnackBar(error.message, null, {
                duration: 3000
            });
        });

    }

    logout() {
        this.afAuth.auth.signOut();
    }


}
