import {NgModule} from '@angular/core';
import {
    MatButtonModule, MatCardModule,
    MatCheckboxModule, MatDialogModule,
    MatListModule,
    MatNativeDateModule, MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
    MatSidenavModule, MatSortModule, MatTableModule, MatTabsModule,
    MatToolbarModule, MatSnackBarModule
} from '@angular/material';
import {MatIconModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material';

@NgModule({
    imports: [MatButtonModule, MatIconModule, MatInputModule, MatSnackBarModule,
        MatFormFieldModule, MatDatepickerModule, MatNativeDateModule,
        MatCheckboxModule, MatSidenavModule, MatToolbarModule, MatListModule,
        MatTabsModule, MatCardModule, MatSelectModule, MatProgressSpinnerModule, MatDialogModule,
        MatTableModule, MatSortModule, MatPaginatorModule],
    exports: [MatButtonModule, MatIconModule, MatInputModule, MatSnackBarModule,
        MatFormFieldModule, MatDatepickerModule, MatNativeDateModule,
        MatCheckboxModule, MatSidenavModule, MatToolbarModule, MatListModule,
        MatTabsModule, MatCardModule, MatSelectModule, MatProgressSpinnerModule, MatDialogModule,
        MatTableModule, MatSortModule, MatPaginatorModule]
})
export class MaterialModule {

}
