import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Observable} from 'rxjs';
import * as fromRoot from '../../app.reducer';
import {Store} from '@ngrx/store';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    @Output() sidenavToggle = new EventEmitter<void>();
    isAuthenticated$: Observable<boolean>;


    constructor(private authService: AuthService, private store: Store<fromRoot.State>) {
    }

    ngOnInit() {
        this.isAuthenticated$ = this.store.select(fromRoot.getIsAuthenticated);
    }

    onToggleSidenav() {
        this.sidenavToggle.emit();
    }

    onLogout() {
        this.authService.logout();
    }


}
