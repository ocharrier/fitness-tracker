import {Component, OnInit, EventEmitter, Output, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from '../../auth/auth.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../app.reducer';

@Component({
    selector: 'app-sidenav-list',
    templateUrl: './sidenav-list.component.html',
    styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {
    @Output() closeSidenav = new EventEmitter<void>();
    isAuthenticated$: Observable<boolean>;

    constructor(private authService: AuthService, private store: Store<fromRoot.State>) {
    }

    ngOnInit() {
        this.isAuthenticated$ = this.store.select(fromRoot.getIsAuthenticated);
    }

    onClose() {
        this.closeSidenav.emit();
    }

    onLogout() {
        this.authService.logout();
        this.onClose();
    }

}
