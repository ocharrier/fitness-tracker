import {Subject} from 'rxjs';
import {MatSnackBar} from '@angular/material';
import {Injectable} from '@angular/core';

@Injectable()
export class UiService {
    loadingStateChanged = new Subject<boolean>();


    constructor(private snackBar: MatSnackBar) {
    }

    showSnackBar(message, action, config) {
        this.snackBar.open(message, action, config);
    }
}
