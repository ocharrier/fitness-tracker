export interface Exercise {
    id: string;
    name: string;
    duration: number;
    calories: number;
    // Optional
    date?: Date;
    // Optional , list of value accepted
    state?: 'completed' | 'cancelled' | null;

}
