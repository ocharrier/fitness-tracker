import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Exercise} from '../exercise.model';
import {TrainingService} from '../training.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

import * as fromTraining from '../training.reducer';
import {Store} from '@ngrx/store';

@Component({
    selector: 'app-past-training',
    templateUrl: './past-training.component.html',
    styleUrls: ['./past-training.component.css']
})
export class PastTrainingComponent implements OnInit, AfterViewInit {

    displayedColumns = ['date', 'name', 'duration', 'calories', 'state'];
    dataSource = new MatTableDataSource<Exercise>();


    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private trainingService: TrainingService, private store: Store<fromTraining.State>) {
    }

    ngOnInit() {
        this.trainingService.fetchFinishedExercises();
        this.store.select(fromTraining.getFinishedExercises).subscribe((exercises: Exercise[]) => {
            // console.log(exercises);
            this.dataSource.data = exercises;
        });
    }

    ngAfterViewInit(): void {
        // Need to wait for fetched data
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    doFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }


}
