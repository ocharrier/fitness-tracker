import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TrainingComponent} from './training.component';
import {AuthGuard} from '../auth/auth.guard';

const routes: Routes = [
    // path appended to path given in app-routing.module.ts, contains already training
    {path: '', component: TrainingComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule
    ]
})
export class TrainingRoutingModule {

}
