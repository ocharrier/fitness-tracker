import {Action} from '@ngrx/store';
import {Exercise} from './exercise.model';

export const SET_AVAILABLE_EXERCISES = '[training] SET_AVAILABLE_EXERCISES';
export const SET_FINISHED_EXERCISE = '[training] SET_FINISHED_EXERCISE';
export const START_TRAINING = '[training] START TRAINING';
export const STOP_TRAINING = '[training] STOP TRAINING';

export class SetAvailableExercises implements Action {
    readonly type = SET_AVAILABLE_EXERCISES;

    constructor(public payload: Exercise[]) {

    }
}

export class SetFinishedExercise implements Action {
    readonly type = SET_FINISHED_EXERCISE;

    constructor(public payload: Exercise[]) {

    }
}

export class StartTraining implements Action {
    readonly type = START_TRAINING;

    constructor(public payload: string) {

    }
}

export class StopTraining implements Action {
    readonly type = STOP_TRAINING;

}

export type TrainingActions = SetAvailableExercises | SetFinishedExercise | StartTraining | StopTraining ;
