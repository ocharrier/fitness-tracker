import {Exercise} from './exercise.model';
import {Injectable} from '@angular/core';
import {Subscription} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import {UiService} from '../shared/ui.service';
import {Store} from '@ngrx/store';
import * as fromTraining from './training.reducer';
import * as UI from '../shared/ui.actions';
import * as Training from './training.actions';


@Injectable()
export class TrainingService {

    private fbSubs: Subscription[] = [];

    constructor(private db: AngularFirestore,
                private uiService: UiService,
                private store: Store<fromTraining.State>) {
    }


    fetchAvailableExercises() {
        this.store.dispatch(new UI.StartLoading());
        this.fbSubs.push(this.db.collection('availableExercises').snapshotChanges().pipe(map(docArray => {
            return docArray.map(doc => {
                return {
                    id: doc.payload.doc.id,
                    name: doc.payload.doc.data()['name'],
                    duration: doc.payload.doc.data()['duration'],
                    calories: doc.payload.doc.data()['calories'],
                };
            });
        })).subscribe((exercises: Array<Exercise>) => {
            this.store.dispatch(new UI.StopLoading());
            this.store.dispatch(new Training.SetAvailableExercises(exercises));
        }, error => {
            this.uiService.showSnackBar('Fetching Failed', null, {
                duration: 3000
            });
            this.store.dispatch(new UI.StopLoading());

            console.log(error);
        }));
    }

    fetchFinishedExercises() {
        this.fbSubs.push(this.db.collection('finishedExercises').valueChanges().subscribe((exercises: Exercise[]) => {
            this.store.dispatch(new Training.SetFinishedExercise(exercises));
        }));
    }

    cancelSubscriptions() {
        this.fbSubs.forEach(sub => sub.unsubscribe());
    }

    startExercise(id: string) {
        this.store.dispatch(new Training.StartTraining(id));
    }

    completeExercise() {
        this.store.select(fromTraining.getActiveExercise).pipe(take(1)).subscribe(exercise => {
            this.addDataToDatabase({
                ...exercise,
                date: new Date(),
                state: 'completed'
            });
            this.store.dispatch(new Training.StopTraining());
        });

    }

    cancelExercise(progress: number) {
        this.store.select(fromTraining.getActiveExercise).pipe(take(1)).subscribe(exercise => {
            this.addDataToDatabase({
                ...exercise,
                duration: exercise.duration * progress / 100,
                calories: exercise.calories * progress / 100,
                date: new Date(),
                state: 'cancelled'
            });
            this.store.dispatch(new Training.StopTraining());
        });

    }

    private addDataToDatabase(exercise: Exercise) {
        this.db.collection('finishedExercises').add(exercise).then();
    }
}
